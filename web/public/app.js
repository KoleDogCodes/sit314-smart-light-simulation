const args = window.location.href.split('/');
const ROOM_ID = args[args.length - 1];
const MAIN_PAGE = args[args.length - 1] == '';
const MAX_ROOMS = 50;
const MAX_LIGHTS = 10;
const MAX_NODES = 10;

//Connect to socket.io server
const socket = io('https://smart-light-socket.herokuapp.com/');

socket.on('connect', () => {
    $('#conn_status').attr('type', 'success');
    $('#conn_status').text('CONNECTED TO SERVER');
    socket.emit('connection_status', 'ping');
});

socket.on('disconnect', () => {
    $('#conn_status').attr('type', 'failed');
    $('#conn_status').text('DISCONNECTED FROM SERVER');
});

// handle the event sent with socket.send()
socket.on('connection_status', data => {
    console.log(data);
    $('title').text('Room ' + ROOM_ID.toUpperCase());
});

socket.on('light_event', data => {
    console.log(data);
    
    if (MAIN_PAGE) updateTable(data);
    else updateLight(data);
});

function updateTable(data) {
    const rowActivity = $(`.room_${data.head_id}[activity]`);
    rowActivity.text(`${new Date().toDateString() + ', ' + new Date().toLocaleTimeString()}`);
}

function updateLight(data) {
    //Toggle all lights
    if (data.node_id == "*" && data.type == "ACTION" && ROOM_ID.toLowerCase() == data.head_id.toLowerCase()) {
        const action = data.light_action.split("=")[1];

        //Change all the images
        $(`img`).attr('src', `./${action}.jpg`);
    }

    //Toggle all lights
    if (data.node_id != "*" && data.type == "ACTION" && ROOM_ID.toLowerCase() == data.head_id.toLowerCase() && data.light_action.startsWith("*")) {
        const action = data.light_action.split("=")[1];

        //Change all the images
        for (var i = 0; i < MAX_LIGHTS; i++) {
            document.getElementById(`light_${data.node_id}.${i}`).setAttribute('src', `./${action}.jpg`);
        }
    }
}

function generateLightHTMLCode(row) {
    var body = "";
    for (var i = 0; i < MAX_LIGHTS; i++) {
        body += `<td> 
                    <img class="light_icon" src="./off.jpg" id="light_${row}.${i}">  
                 </td>`; 
    }

    return `<tr> ${body} </tr>`;
}

//Populate table with 50 rooms
if (MAIN_PAGE) {
    for (var i = 0; i < MAX_ROOMS; i++) {
        $('tbody').append(`
        <tr class="room_B${i + 1}" onclick="window.location.href='./B${i + 1}'">
            <td class="room_B${i + 1}"> Room <b>B${i + 1}</b> </td>
            <td class="room_B${i + 1}"> </td>
            <td class="room_B${i + 1}" activity> Off </td>
        </tr>
        `);
    }
}

//Populate room page with light images
else {
    for (var i = 0; i < MAX_NODES; i++) {
        $('tbody').append(generateLightHTMLCode(i));
    }
}

// var light_data = {};

//     for (var room = 0; room < MAX_ROOMS; room++) {
//         light_data[room] = [];
//         for (var node = 0; node < MAX_NODES; node++) {
//             light_data[room][node] = [];
//             light_data[room][node].push([1,2,3]);
//         }
//     }

// console.log(light_data);

